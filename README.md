Getting Started with this Example
=================================

I created a handful of dummy users and groups.

Create and import the MySQL database:

From mysql prompt:
```
mysql> create database cake_feeds_development;
```

From shell prompt:
```
$ cd /path/to/repo
$ mysql -u root cake_feeds_development < db/cake_feeds.sql
```

Create a Virtual Host to run this app - I will leave this to you.

Then in your browser just navigate to the root `/`.

You can create some dummy feed data for a user you select from the drop down, along with 2 different activity types.

Create some data and then choose that users feed to view.

Things TODO and Think About
===========================

1. Dealing with items that have been deleted, e.g. there is a group-message activity item for a group which has been deleted. 
Solution: in the phase when you pull out the IDs to grab the names you check if you did NOT get back a record (e.g. the group name) for an activity item you have in your possession,
if you don't get a record back you remove the activity item from the DB and the current "feed data" set.

2. If you're deleting items during the "gathering" phase than it complicates pagination, that is you guaranteed the client you would give them back 20 entries and you just deleted 1
than it means you have to dive back into the DB to get an item to satisfy the count=20 request. I would argue that pagination for a feed is bullshit. Numerical pagination that is (pages 1, 2, 3..) No one
cares they their feed is "6 pages" long. But you *could* have a "Load More" link which attempts to load the next "20" entries but the 20 is a best effort, you might only get back 16, because
you ended up ditching 4. I don't think the user would notice or care.

3. Increasing Perceived Performance: this feed is fast now but you might notice it slowing down later (but realistically this will scale out pretty good). But anyways
you could load the feed by AJAX. So render the rest of the page and then just do an asynchronous AJAX call to grab the feed and then just inject it into the page. This is what Facebook does.

