<?php

App::uses('AppController', 'Controller');

class FeedsController extends AppController {
	public $name = 'Feeds';
	public $uses = array('Feed', 'User', 'Group', 'GroupMessage');
	
	public function index() {
  }
  
  public function landing() {
    $users = $this->User->find('all');
    $this->set('users', $users);
    $groups = $this->Group->find('all');
    $this->set('groups', $groups);
    
    // display a users feed?
    $have_user_id = array_key_exists('user_id', $this->request->query);
    $view_feed = array_key_exists('view-feed', $this->request->query);
    if($view_feed && $have_user_id) {
      $feed_data = $this->Feed->fetch_activities($this->request->query['user_id']);
      $this->set('feed_data', $feed_data);
    }

  }
  
  public function create() {
    if($this->request->is('post')) {
      $activity_kind = $this->request->data['kind'];
      
      if($activity_kind == 'friendship') {
        $activity_data = array(
          'kind' => 'friendship',
          'user_1_id' => intval($this->request->data['user_1_id']),
          'user_2_id' => intval($this->request->data['user_2_id'])
        );
        
        $record = array(
          'user_id' => $this->request->data['feed_user_id'],
          'data' => json_encode($activity_data)
        );
        $this->Feed->create();
        $this->Feed->save($record);
        
        $this->Session->setFlash('Activity created!.');
        $this->redirect(array('action' => 'landing'));
      } /* friendship */
      
      if($activity_kind == 'group-message') {
        
        // group message data
        $message_data = array(
          'group_id' => $this->request->data['group_id'],
          'user_id' => $this->request->data['user_id'],
          'subject' => $this->request->data['subject'],
          'message' => $this->request->data['message']
        );
        $this->GroupMessage->create();
        $this->GroupMessage->save($message_data);
        
        // create a feed activity
        $activity_data = array(
          'kind' => 'group-message',
          'group_id' => intval($this->request->data['group_id']),
          'user_id' => intval($this->request->data['user_id']),
          'subject' => $this->request->data['subject']
        );
        
        $record = array(
          'user_id' => $this->request->data['feed_user_id'],
          'data' => json_encode($activity_data)
        );
        
        $this->Feed->create();
        $this->Feed->save($record);

        $this->Session->setFlash('Activity created!.');
        $this->redirect(array('action' => 'landing'));
      } /* group-message */
    } else {
      die('Only POST allowed');
    }
  }
}
?>