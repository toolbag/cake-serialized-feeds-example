<?php

class Feed extends AppModel {
    public $name = 'Feed';
    
    public function fetch_activities($user_id) {
      $records = $this->find('all',
        array(
          'conditions' => array('user_id' => $user_id),
          'order' => array('id DESC')
        )
      );

      $group_ids = array();
      $user_ids = array();
      $feed_data = array();
      
      $user_map = array();
      $group_map = array();
      
      // Pass 1: extract all the IDs of elements we need to fetch names for
      foreach($records AS $record) {
        $activity = array();
        $activity['created'] = $record['Feed']['created'];
        $activity['data'] = json_decode($record['Feed']['data'], TRUE);
        $activity['kind'] = $activity['data']['kind'];
        
        // if its a friendship then we need to fetch both user names
        if($activity['data']['kind'] == 'friendship') {
          $user_ids[] = intval($activity['data']['user_1_id']);
          $user_ids[] = intval($activity['data']['user_2_id']);
        }
        
        // if its a group message than we need to fetch the group name
        // and the user who posted the message
        if($activity['data']['kind'] == 'group-message') {
          $group_ids[] = intval($activity['data']['group_id']);
          $user_ids[] = intval($activity['data']['user_id']);
        }
        $feed_data[] = $activity;
      }
      
      // Fetch the names of Users and Groups we need
      // We use raw SQL so we can bypass the Model layer which probably
      // does too much heuristics to inflate the data, all we need is the raw data
      if(count($user_ids) > 0) {
        $unique_user_ids = array_unique($user_ids);
        $raw_sql = sprintf("SELECT id, name FROM users WHERE id IN (%s)", implode(",", $unique_user_ids));
        $user_records = $this->query($raw_sql);
        foreach($user_records AS $row) {
          $id = $row['users']['id'];
          $name = $row['users']['name'];
          $user_map[$id] = $name;
        }
      }

      if(count($group_ids) > 0) {
        $unique_group_ids = array_unique($group_ids);
        $raw_sql = sprintf("SELECT id, name FROM groups WHERE id IN (%s)", implode(",", $unique_group_ids));
        $group_records = $this->query($raw_sql);
        foreach($group_records AS $row) {
          $id = $row['groups']['id'];
          $name = $row['groups']['name'];
          $group_map[$id] = $name;
        }
      }
      
      $complete_feed_data = array(
        'activities' => $feed_data,
        'user_map' => $user_map,
        'group_map' => $group_map
      );

      return($complete_feed_data);
    }
}

?>