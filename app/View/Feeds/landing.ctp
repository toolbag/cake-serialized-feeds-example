<?php echo $this->Session->flash(); ?>
<h3>Create dummy Feed Data</h3>

<form action="/feeds/create" method="post">
  <input type="hidden" name="create-entry" value="true" />
  
  In feed of:
  <select name="feed_user_id">
    <?php 
      foreach($users as $user) {
        echo '<option value="' . $user['User']['id'] . '">' . $user['User']['name']. '</option>';
      }
    ?>
  </select>
    
  create a:
  
  <select name="kind" id="activity_kind">
    <option value="">-- Choose --</option>
    <option value="friendship">Friendship</option>
    <option value="group-message">Group message</option>
  </select>
  
  <div id="friendship" class="activity_form">
    Between
    
    user1:
    <select name="user_1_id">
      <?php 
        foreach($users as $user) {
          echo '<option value="' . $user['User']['id'] . '">' . $user['User']['name']. '</option>';
        }
      ?>
    </select>
    and
    
    user2:
    <select name="user_2_id">
      <?php 
        foreach($users as $user) {
          echo '<option value="' . $user['User']['id'] . '">' . $user['User']['name']. '</option>';
        }
      ?>
    </select>
    
  </div>

  <div id="group-message" class="activity_form">
    In Group:
    <select name="group_id">
      <?php 
        foreach($groups as $group) {
          echo '<option value="' . $group['Group']['id'] . '">' . $group['Group']['name']. '</option>';
        }
      ?>
    </select>
    
    <br />
    Title: <input type="text" name="subject" length="100" placeholder="Subject" />

    <br />
    Message: <input type="text" name="message" placeholder="Message" />

    <br />
    Posted by:
    <select name="user_id">
      <?php 
        foreach($users as $user) {
          echo '<option value="' . $user['User']['id'] . '">' . $user['User']['name']. '</option>';
        }
      ?>
    </select>
    
  </div>
  
  <input type="submit" value="Go" />
</form>

<br />
<br />
<br />
<br />
<h3>View User Feed</h3>
<form action="/" method="get">
  <input type="hidden" name="view-feed" value="true" />
  View feed of:
  <select name="user_id">
    <?php 
      $user_id = 0;
      if(isset($this->request->query['user_id'])) {
        $user_id = intval($this->request->query['user_id']);
      }
      foreach($users as $user) {
        $selected = $user['User']['id'] == $user_id ? "selected=\"selected\"" : "";
        echo '<option value="' . $user['User']['id'] . '"' . $selected . '>' . $user['User']['name']. '</option>';
      }
    ?>
  </select>
  <input type="submit" value="Go" />

</form>

<hr />
<?php
if(isset($feed_data)) {
  $activities = $feed_data['activities'];
  $group_map = $feed_data['group_map'];
  $user_map = $feed_data['user_map'];
  
  if(count($activities) > 0) {
    echo "<ul class=\"feed\">";
    foreach($activities AS $activity) {
      echo "<li>";
      echo "On " . $activity['created'] . "<br />";

      if($activity['kind'] == 'friendship') {
        $user_1_id = intval($activity['data']['user_1_id']);
        $user_2_id = intval($activity['data']['user_2_id']);
        $user_1_name = $user_map[$user_1_id];
        $user_2_name = $user_map[$user_2_id];
        echo $user_1_name . " became friends with " . $user_2_name;
      }

      if($activity['kind'] == 'group-message') {
        $group_id = intval($activity['data']['group_id']);
        $user_id = intval($activity['data']['user_id']);
        $user_name = $user_map[$user_id];
        $group_name = $group_map[$group_id];
        $subject = $activity['data']['subject'];
        echo $user_name . " posted in the " . $group_name . " group: <br />";
        echo "<em>" . $subject . "</em>";
      }
      echo "</li>";
    }
    echo "</ul>";
  } else {
    echo "This user has no activities!";
  }
} else {
  echo "Choose a user from above";
}
?>

<script>
  $(document).ready(function() {
    var activity_kind = $('#activity_kind');
    activity_kind.on('change', function(e) {
      var value = $.trim($(this).val());
      if(value != '') {
        $('div.activity_form').hide();
        $('#' + value).show();
      }
    });
  });
</script>